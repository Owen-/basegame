﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : MonoBehaviour
{
    public SelectableGameObjectList Levels;
    public GameEvent SpawnEvent;
    public GameEvent ReturnToMenuEvent;
    public IntReference Score;

    private bool isReversed = false;
    private int index;

    private void Awake()
    {
        SpawnEvent.Event += Spawn;
        ReturnToMenuEvent.Event += Spawn;
        Spawn();
    }

    private void OnDestroy()
    {
        SpawnEvent.Event -= Spawn;
        ReturnToMenuEvent.Event -= Spawn;
    }

    public void Spawn()
    {
        if (Levels.Selected != null)
        {
            Destroy(Levels.Selected.gameObject);
        }

        index = Score.Value % ((Levels.Count-1) * 2);
        if (index > Levels.Count-1)
        {
            index = (Levels.Count * 2 - (index+2));
        }
        Levels.Selected = Instantiate(Levels[index], transform);
    }
}

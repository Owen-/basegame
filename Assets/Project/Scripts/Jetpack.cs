﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jetpack : MonoBehaviour
{
    public Rigidbody2D Rigidbody2D;
    public FloatReference UpThrust;
    public FloatReference DownThrust;
    public FloatReference HorizontalSpeed;
    public IntEvent PointsScored;
    public GameEvent StartEvent;
    public GameEvent SpawnLevelEvent;
    public GameEvent EndEvent;
    public GameEvent ReturnToMenuEvent;
    public ResetPosition ResetPosition;
    public KeyCode UpKey;
    public SpriteRenderer SpriteRenderer;
    public Color InitialColour;
    public Color DeadColour;
    public GameObject JetpackTrail;

    private bool started = false;
    private float initialHorizontalValue;

    private void Awake()
    {
        ResetPosition.Initialize();
        initialHorizontalValue = HorizontalSpeed.Value;
        Time.timeScale = 0.5f;

        StartEvent.Event += HandleStartEvent;
        EndEvent.Event += HandleEndEvent;
        ReturnToMenuEvent.Event += HandleReturnToMenu;
    }

    private void OnDestroy()
    {
        StartEvent.Event -= HandleStartEvent;
        EndEvent.Event -= HandleEndEvent;
        ReturnToMenuEvent.Event -= HandleReturnToMenu;
    }

    private void HandleReturnToMenu()
    {
        ResetPosition.Reset();
        SpriteRenderer.color = InitialColour; ///TODO
    }

    private void HandleEndEvent()
    {
        started = false;
        tag = "Untagged";
        SpriteRenderer.color = DeadColour; ///TODO
        JetpackTrail.SetActive(false);
    }

    private void HandleStartEvent()
    {
        HorizontalSpeed.Value = initialHorizontalValue;
        ResetPosition.Initialize();
        started = true;
        Rigidbody2D.isKinematic = false;
        tag = "Player"; //TODO
        JetpackTrail.SetActive(false);
    }

    private void Update()
    {
        if (started)
        {
            Move();
        }
        else
        {
            JetpackTrail.SetActive(false);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" && started) //TODO
        {
            HorizontalSpeed.Value *= -1f;
            PointsScored.Raise(1);
            SpawnLevelEvent.Raise();
        }
    }

    private void Move ()
    {
		if (Input.GetKey(UpKey))
        {
            JetpackTrail.SetActive(true);
            Rigidbody2D.AddForce(Vector2.up * UpThrust, ForceMode2D.Force);
        }
        else
        {
            JetpackTrail.SetActive(false);
            Rigidbody2D.AddForce(Vector2.down * DownThrust, ForceMode2D.Force);
        }

        Vector3 vel = Rigidbody2D.velocity;

        vel.x = HorizontalSpeed;

        Rigidbody2D.velocity = vel;
	}
}
